<?php
/*
 * Provide a select options component to Webform.
 * 
 * Via a callback to the function _formref_get_view_options.
 *
 * @return array of list items for webform
 * @author Mathias Døhl Sørensen
 */
function formref_webform_select_options_info() {
	
	$items = array();
	
	// Check if the views module is enabled
	if (module_exists('views')) {
		// Create a group header option, that can't be selected. 
		// Tell that it is the views list.
		$items['Views'] = array('title' => array());
		// Get the views list. (Only enabled)
		$views_list = views_get_enabled_views();
		// Go through the views list
		foreach ($views_list as $id => $view) {
			// Create an view option 
			$items[$view->name] = array(
				// The title of the option
				'title' => t('- '.$view->human_name),
				// The funktion to run when selected
				'options callback' => '_formref_get_view_options',
				// The argument to pass
				'options arguments' => $view->name,
			);
		}
	}
	
  return $items;
}

/*
 * Options callback for formref_webform_select_options_info().
 * 
 * Get the list of titles, from the selected view.
 * The list is being displayed as a select opstion.
 *
 * @return array of items to populate the select list with.
 * @author Mathias Døhl Sørensen
 */
function _formref_get_view_options($component, $flat, $filter, $my_argument) {
	// Get the selected view
	$view = views_get_view($my_argument, true);
	$view->execute();
	$items = array();
	
	// Go through the items in the selected view
	foreach ($view->result as $item) {
	  $full_node = node_load($item->nid);
	  // Put the title in a selection option
	  $items[$item->nid] = $full_node->title;
	}
	
	return $items;
}